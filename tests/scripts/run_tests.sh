#!/bin/bash
set -euxo pipefail

TOPDIR=$1

# Print some information about the machine
df -h
free -h
rpm -qa
find /etc/yum.repos.d -type f -print -exec cat {} \;

# Run only basic smoke tests.
# Our test-suite is currently very thorough and it builds a lot of images.
# On some runs, TFT was able to build one image for 25 minutes. Our test
# suite builds more than 25 images which means that the full test-suite
# can run for more than 10 hours. That's just too slow and since we are also
# able to run downstream tests on our much faster upstream infrastructure.
# I think that it's enough to do just smoke tests here.
/usr/libexec/tests/osbuild-composer/base_tests.sh
